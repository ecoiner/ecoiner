package repositories;

import java.util.List;

/**
 * Interface
 * @author renato
 *
 * @param <T>
 */
public interface IRepository<T> {
	/**
	 * Guarda un objeto
	 * @param t
	 */
	void save(T t);
	
	/**
	 * Edita un objeto
	 * @param t
	 */
	void edit(T t);
	
	/**
	 * Elimina un objeto
	 * @param t
	 */
	void remove(T t);
	
	/**
	 * Retorna una lista de objetos
	 * @return
	 */
	List<T> findAll();
	
	/**
	 * Retorna un objeto buscado por id
	 * @param id
	 * @return
	 */
	T findById(Long id);
}
