package repositories;

import model.Usuario;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class UsuarioRepository implements IRepository<Usuario> {

    private EntityManager manager;
	
	public UsuarioRepository(){
		manager = this.getEntityManager();
	}
	
	@Override
	public void save(Usuario u) {
		// TODO Auto-generated method stub
		this.manager.persist(u);
	}

	@Override
	public void edit(Usuario u) {
		// TODO Auto-generated method stub
		this.manager.merge(u);
	}

	@Override
	public void remove(Usuario t) {
		// TODO Auto-generated method stub
		this.manager.remove(t);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		Query query = this.manager.createQuery("select u from Usuario u");
		return query.getResultList();
	}

	@Override
	public Usuario findById(Long id) {
		// TODO Auto-generated method stub
		return this.manager.find(Usuario.class, id);
	}
	
	/**
	 * Instancia o EM
	 * 
	 * @return
	 */
	private final EntityManager getEntityManager() {
//		FacesContext fc = FacesContext.getCurrentInstance();
//		ExternalContext ec = fc.getExternalContext();
//		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
//		EntityManager manager = (EntityManager) request.getAttribute("EntityManager");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ecoiner");
        EntityManager manager = emf.createEntityManager();
		return manager;
	}

    public Usuario login(String nombreUsuario, String contrasenia) {
        Query query = manager.createQuery("SELECT u FROM Usuario u where u.nombreUsuario=:nombreUsuario and u.contrasenia=:contrasenia");
        query.setParameter("nombreUsuario", nombreUsuario);
        query.setParameter("contrasenia", contrasenia);
        Usuario u=(Usuario) query.getSingleResult();
        return u;
    }
}