package controller;

import model.Usuario;
import repositories.UsuarioRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class LoginBean {
	
	private Usuario usuario;
	private UsuarioRepository usuarioRepository = new UsuarioRepository();

    @PostConstruct
    public void init(){
        usuario = new Usuario();
    }

    public Usuario login(){
        return usuarioRepository.login(usuario.getNombreUsuario(), usuario.getContrasenia());
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
