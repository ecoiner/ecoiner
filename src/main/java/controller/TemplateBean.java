package controller;

import model.Usuario;
import repositories.UsuarioRepository;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class TemplateBean {
	
	private Usuario usuario = new Usuario();
	private UsuarioRepository usuarioRepository = new UsuarioRepository();

    public Usuario login(){
//        RequestContext.getCurrentInstance().openDialog("mensaje");
//        return null;
        return usuarioRepository.login(usuario.getNombreUsuario(), usuario.getContrasenia());
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
