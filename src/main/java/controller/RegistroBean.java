package controller;

import model.Usuario;
import repositories.UsuarioRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.Date;

@ManagedBean
@RequestScoped
public class RegistroBean {
	
	private Usuario usuario;
	private UsuarioRepository usuarioRepository = new UsuarioRepository();

    @PostConstruct
    public void init(){
        usuario = new Usuario();
    }

    public void registrar(){
        usuario.setDateRegistro(new Date());
        usuario.setEstadoCuenta("ACTIVO");
        usuario.setDtype("dtype");

        usuarioRepository.save(usuario);
        System.out.println("guardado satisfactoriamente...");
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
