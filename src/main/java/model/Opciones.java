/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author renato
 */
@Entity
@Table(name = "opciones")
@NamedQueries({
    @NamedQuery(name = "Opciones.findAll", query = "SELECT o FROM Opciones o")})
public class Opciones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_opciones")
    private Long idOpciones;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "resultado")
    private String resultado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOpciones", fetch = FetchType.LAZY)
    private List<RegistroApuestas> registroApuestasList;
    @JoinColumn(name = "id_apuesta", referencedColumnName = "id_apuesta")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Apuesta idApuesta;

    public Opciones() {
    }

    public Opciones(Long idOpciones) {
        this.idOpciones = idOpciones;
    }

    public Opciones(Long idOpciones, String nombre, String resultado) {
        this.idOpciones = idOpciones;
        this.nombre = nombre;
        this.resultado = resultado;
    }

    public Long getIdOpciones() {
        return idOpciones;
    }

    public void setIdOpciones(Long idOpciones) {
        this.idOpciones = idOpciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public List<RegistroApuestas> getRegistroApuestasList() {
        return registroApuestasList;
    }

    public void setRegistroApuestasList(List<RegistroApuestas> registroApuestasList) {
        this.registroApuestasList = registroApuestasList;
    }

    public Apuesta getIdApuesta() {
        return idApuesta;
    }

    public void setIdApuesta(Apuesta idApuesta) {
        this.idApuesta = idApuesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOpciones != null ? idOpciones.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Opciones)) {
            return false;
        }
        Opciones other = (Opciones) object;
        if ((this.idOpciones == null && other.idOpciones != null) || (this.idOpciones != null && !this.idOpciones.equals(other.idOpciones))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Opciones[ idOpciones=" + idOpciones + " ]";
    }
    
}
