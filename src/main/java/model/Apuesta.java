/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author renato
 */
@Entity
@Table(name = "apuesta")
@NamedQueries({
    @NamedQuery(name = "Apuesta.findAll", query = "SELECT a FROM Apuesta a")})
public class Apuesta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_apuesta")
    private Long idApuesta;
    @Basic(optional = false)
    @Column(name = "tipo_apuesta")
    private String tipoApuesta;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @Column(name = "cantidad_opciones")
    private String cantidadOpciones;
    @Basic(optional = false)
    @Column(name = "cantidad_ganadoras")
    private String cantidadGanadoras;
    @Basic(optional = false)
    @Column(name = "respaldo")
    private String respaldo;
    @Basic(optional = false)
    @Column(name = "or")
    private String or;
    @Basic(optional = false)
    @Column(name = "pertenece_a")
    private String perteneceA;
    @Basic(optional = false)
    @Column(name = "id_pertenece")
    private long idPertenece;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idApuesta", fetch = FetchType.LAZY)
    private List<Opciones> opcionesList;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usuario idUsuario;
    @JoinColumn(name = "id_evento", referencedColumnName = "id_evento")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Evento idEvento;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "apuesta", fetch = FetchType.LAZY)
    private Resultado resultado;

    public Apuesta() {
    }

    public Apuesta(Long idApuesta) {
        this.idApuesta = idApuesta;
    }

    public Apuesta(Long idApuesta, String tipoApuesta, String estado, String cantidadOpciones, String cantidadGanadoras, String respaldo, String or, String perteneceA, long idPertenece) {
        this.idApuesta = idApuesta;
        this.tipoApuesta = tipoApuesta;
        this.estado = estado;
        this.cantidadOpciones = cantidadOpciones;
        this.cantidadGanadoras = cantidadGanadoras;
        this.respaldo = respaldo;
        this.or = or;
        this.perteneceA = perteneceA;
        this.idPertenece = idPertenece;
    }

    public Long getIdApuesta() {
        return idApuesta;
    }

    public void setIdApuesta(Long idApuesta) {
        this.idApuesta = idApuesta;
    }

    public String getTipoApuesta() {
        return tipoApuesta;
    }

    public void setTipoApuesta(String tipoApuesta) {
        this.tipoApuesta = tipoApuesta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCantidadOpciones() {
        return cantidadOpciones;
    }

    public void setCantidadOpciones(String cantidadOpciones) {
        this.cantidadOpciones = cantidadOpciones;
    }

    public String getCantidadGanadoras() {
        return cantidadGanadoras;
    }

    public void setCantidadGanadoras(String cantidadGanadoras) {
        this.cantidadGanadoras = cantidadGanadoras;
    }

    public String getRespaldo() {
        return respaldo;
    }

    public void setRespaldo(String respaldo) {
        this.respaldo = respaldo;
    }

    public String getOr() {
        return or;
    }

    public void setOr(String or) {
        this.or = or;
    }

    public String getPerteneceA() {
        return perteneceA;
    }

    public void setPerteneceA(String perteneceA) {
        this.perteneceA = perteneceA;
    }

    public long getIdPertenece() {
        return idPertenece;
    }

    public void setIdPertenece(long idPertenece) {
        this.idPertenece = idPertenece;
    }

    public List<Opciones> getOpcionesList() {
        return opcionesList;
    }

    public void setOpcionesList(List<Opciones> opcionesList) {
        this.opcionesList = opcionesList;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Evento getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Evento idEvento) {
        this.idEvento = idEvento;
    }

    public Resultado getResultado() {
        return resultado;
    }

    public void setResultado(Resultado resultado) {
        this.resultado = resultado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idApuesta != null ? idApuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Apuesta)) {
            return false;
        }
        Apuesta other = (Apuesta) object;
        if ((this.idApuesta == null && other.idApuesta != null) || (this.idApuesta != null && !this.idApuesta.equals(other.idApuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Apuesta[ idApuesta=" + idApuesta + " ]";
    }
    
}
