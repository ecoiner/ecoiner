/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author renato
 */
@Entity
@Table(name = "registro_apuestas")
@NamedQueries({
    @NamedQuery(name = "RegistroApuestas.findAll", query = "SELECT r FROM RegistroApuestas r")})
public class RegistroApuestas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro")
    private Long idRegistro;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @Basic(optional = false)
    @Column(name = "cuota")
    private String cuota;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private long idUsuario;
    @Basic(optional = false)
    @Column(name = "id_documento")
    private long idDocumento;
    @JoinColumn(name = "id_opciones", referencedColumnName = "id_opciones")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Opciones idOpciones;

    public RegistroApuestas() {
    }

    public RegistroApuestas(Long idRegistro) {
        this.idRegistro = idRegistro;
    }

    public RegistroApuestas(Long idRegistro, String valor, String cuota, long idUsuario, long idDocumento) {
        this.idRegistro = idRegistro;
        this.valor = valor;
        this.cuota = cuota;
        this.idUsuario = idUsuario;
        this.idDocumento = idDocumento;
    }

    public Long getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Long idRegistro) {
        this.idRegistro = idRegistro;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCuota() {
        return cuota;
    }

    public void setCuota(String cuota) {
        this.cuota = cuota;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(long idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Opciones getIdOpciones() {
        return idOpciones;
    }

    public void setIdOpciones(Opciones idOpciones) {
        this.idOpciones = idOpciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroApuestas)) {
            return false;
        }
        RegistroApuestas other = (RegistroApuestas) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.RegistroApuestas[ idRegistro=" + idRegistro + " ]";
    }
    
}
